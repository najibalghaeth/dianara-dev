/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef BANNERNOTIFICATION_H
#define BANNERNOTIFICATION_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>

#include <QDebug>


class BannerNotification : public QWidget
{
    Q_OBJECT

public:
    explicit BannerNotification(QWidget *parent = 0);
    ~BannerNotification();


signals:
    void updateRequested();
    void bannerCancelled();


public slots:
    void onOk();
    void onCancel();


private:
    QHBoxLayout *m_mainLayout;

    QWidget *m_containerWidget;
    QHBoxLayout *m_containerLayout;

    QLabel *m_iconLabel;
    QLabel *m_descriptionLabel;
    QPushButton *m_okButton;
    QPushButton *m_cancelButton;
};

#endif // BANNERNOTIFICATION_H
