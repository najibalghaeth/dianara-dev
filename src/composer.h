/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef COMPOSER_H
#define COMPOSER_H

#include <QTextEdit>
#include <QFocusEvent>
#include <QLabel>
#include <QPushButton>
#include <QMenu>
#include <QInputDialog>
#include <QMessageBox>
#include <QApplication>
#include <QClipboard>
#include <QMimeData> // Needed in Qt5
#include <QCompleter>
#include <QAbstractItemView> // For QCompleter's popup
#include <QTableView>
#include <QHeaderView>
#include <QTextDocument>
#include <QTextDocumentFragment>
#include <QTextCursor>

#include <QDebug>

#include "globalobject.h"
#include "mischelpers.h"


class Composer : public QTextEdit
{
    Q_OBJECT

public:
    Composer(GlobalObject *globalObject, bool forPublisher,
             QWidget *parent = 0);
    ~Composer();

    void erase();
    void insertLink(QString url, QString title="");
    void requestCompletion(QString partialNick);

    void hideInfoMessage();
    QPushButton *getToolsButton();

    void setPlainPasteEnabled(bool state);


signals:
    void focusReceived();
    void editingFinished();
    void editingCancelled();
    void cancelSavingDraftRequested();
    void focusTitleRequested();

    void nickInserted(QString id, QString name, QString url, QString listType);

    void fileDropped(QString fileUrl);

    void errorHappened(QString errorMessage);


public slots:
    void makeNormal();
    void makeBold();
    void makeItalic();
    void makeUnderline();
    void makeStrikethrough();
    void makeHeader();
    void makeList();
    void makeTable();
    void makePreformatted();
    void makeQuote();
    void makeLink();
    void insertImage();
    void insertLine();

    void insertSymbol(QAction *action);

    void pasteAsPlaintext();

    void insertCompletedNick(QModelIndex nickData);

    void cancelPost();


protected:
    virtual void focusInEvent(QFocusEvent *event);
    virtual void dropEvent(QDropEvent *event);
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void contextMenuEvent(QContextMenuEvent *event);
    virtual void insertFromMimeData(const QMimeData *source);


private:
    QPushButton *m_toolsButton;
    QMenu *m_toolsMenu;
    QMenu *m_symbolsMenu;

    QAction *m_pastePlaintextAction;

    QMenu *m_customContextMenu;

    QCompleter *m_nickCompleter;
    QTableView *m_popupTableView;


    QString m_clickToPostString;
    bool m_forPublisher;


    GlobalObject *m_globalObject;
};

#endif // COMPOSER_H
