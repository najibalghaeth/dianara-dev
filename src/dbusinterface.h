/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef DBUSINTERFACE_H
#define DBUSINTERFACE_H

#include <QObject>
#include <QDBusAbstractAdaptor>

#include <QDebug>


class DBusInterface : public QObject
{
    Q_OBJECT

    // Optional, but makes the D-Bus exported methods look nicer
    Q_CLASSINFO("D-Bus Interface", "org.nongnu.dianara")

public:
    explicit DBusInterface(QObject *parent = 0);
    ~DBusInterface();

signals:

public slots:
    void toggle();
    void post(QString title, QString content);

};

#endif // DBUSINTERFACE_H
