/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef DOWNLOADWIDGET_H
#define DOWNLOADWIDGET_H

#include <QFrame>
#include <QHBoxLayout>
#include <QLabel>
#include <QProgressBar>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>

#include <QDebug>

#include "pumpcontroller.h"


class DownloadWidget : public QFrame
{
    Q_OBJECT

public:
    explicit DownloadWidget(QString fileUrl,
                            PumpController *pumpController,
                            QWidget *parent = 0);
    ~DownloadWidget();

    void resetWidget();
    void updateSuggestedFilename(QString newFilename);


signals:


public slots:
    void downloadAttachment();
    void openAttachment();
    void cancelDownload();
    void completeDownload(QString url);
    void onDownloadFailed(QString url);

    void storeFileData();
    void updateProgressBar(qint64 received, qint64 total);


private:
    QHBoxLayout *m_layout;
    QLabel *m_infoLabel;
    QPushButton *m_openButton;
    QProgressBar *m_progressBar;
    QPushButton *m_downloadButton;
    QPushButton *m_cancelButton;

    QString m_fileUrl;
    QString m_suggestedFilename;

    PumpController *m_pumpController;
    QNetworkReply *m_networkReply;
    QFile m_downloadedFile;
    bool m_downloading;
    bool m_aborted;
};

#endif // DOWNLOADWIDGET_H
