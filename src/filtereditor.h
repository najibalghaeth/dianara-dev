/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef FILTEREDITOR_H
#define FILTEREDITOR_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QIcon>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QListWidget>
#include <QVariantList>
#include <QVariantMap>
#include <QSettings>
#include <QAction>
#include <QCloseEvent>

#include <QDebug>

#include "filterchecker.h"

class FilterEditor : public QWidget
{
    Q_OBJECT

public:
    explicit FilterEditor(FilterChecker *filterChecker,
                          QWidget *parent = 0);
    ~FilterEditor();

    void loadFilters();


signals:


public slots:
    void onFilterTextChanged(QString text);
    void onFilterRowChanged(int row);

    void addFilter();
    void removeFilter();

    void saveFilters();


protected:
    virtual void closeEvent(QCloseEvent *event);
    virtual void hideEvent(QHideEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_topLayout;
    QGroupBox *m_newFilterGroupBox;
    QVBoxLayout *m_middleLayout;
    QGroupBox *m_currentFiltersGroupBox;
    QDialogButtonBox *m_bottomButtonBox;

    QAction *m_closeAction;


    QLabel *m_explanationLabel;

    QComboBox *m_actionTypeComboBox;
    QComboBox *m_filterTypeComboBox;
    QLineEdit *m_filterWordsLineEdit;
    QPushButton *m_addFilterButton;

    QListWidget *m_filtersListWidget;
    QPushButton *m_removeFilterButton;

    QPushButton *m_saveButton;
    QPushButton *m_cancelButton;


    QString m_ruleTemplateString;
    FilterChecker *m_filterChecker;
};

#endif // FILTEREDITOR_H
