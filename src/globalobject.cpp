/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "globalobject.h"


GlobalObject::GlobalObject(QObject *parent) : QObject(parent)
{
    QSettings settings;
    settings.beginGroup("Configuration");

    /////////////////////////////////////////////////////////////////// GENERAL


    ///////////////////////////////////////////////////////////////////// FONTS
    QFont defaultTitleFont;     // 1 point larger
    defaultTitleFont.setPointSize(defaultTitleFont.pointSize() + 1);
    defaultTitleFont.setBold(true);

    QFont defaultContentFont;   // Just the default text size

    QFont defaultCommentsFont;  //  1 point smaller
    defaultCommentsFont.setPointSize(defaultCommentsFont.pointSize() - 1);

    QFont defaultMinorFeedFont; // 2 points smaller
    defaultMinorFeedFont.setPointSize(defaultMinorFeedFont.pointSize() - 2);

    this->syncFontSettings(settings.value("font1",
                                          defaultTitleFont).toString(),
                           settings.value("font2",
                                          defaultContentFont).toString(),
                           settings.value("font3",
                                          defaultCommentsFont).toString(),
                           settings.value("font4",
                                          defaultMinorFeedFont).toString());


    //////////////////////////////////////////////////////////////////// COLORS
    this->colorsList.clear();                           // Defaults:
    colorsList << settings.value("color1", "#CC2030").toString() // Red
               << settings.value("color2", "#5599CC").toString() // Blue
               << settings.value("color3", "#DDCC10").toString() // Yellow
               << settings.value("color4", "#10BB10").toString() // Green
               << settings.value("color5", "#AA77EE").toString() // Purple
               << settings.value("color6").toString();
    // No need to call this->syncColorSettings() here...


    ///////////////////////////////////////////////////////////////// TIMELINES
    int pppMain = qBound(5,
                         settings.value("postsPerPageMain", 20).toInt(),
                         50);
    int pppOther = qBound(1,
                          settings.value("postsPerPageOther", 5).toInt(),
                          30);
    this->syncTimelinesSettings(pppMain,
                                pppOther,
                                settings.value("showDeletedPosts",     false).toBool(),
                                settings.value("hideDuplicatedPosts",  false).toBool(),
                                settings.value("jumpToNewOnUpdate",    false).toBool(),
                                settings.value("mfSnippetsType",       2).toInt(), // Default: Always
                                settings.value("snippetCharLimit",     30).toInt(),
                                settings.value("snippetCharLimitHl",   200).toInt(),
                                settings.value("minorFeedAvatarIndex", 1).toInt(),  // Default: 2nd (32x32)
                                settings.value("minorFeedIconType",    0).toInt()); // Default: No


    ///////////////////////////////////////////////////////////////////// POSTS
    this->syncPostSettings(settings.value("postAvatarIndex",       3).toInt(), // 4th (64x64) by default
                           settings.value("commentAvatarIndex",    1).toInt(), // 2nd (32x32) by default
                           settings.value("postExtendedShares",    true).toBool(),
                           settings.value("postShowExtraInfo",     false).toBool(),
                           settings.value("postHLAuthorComments",  true).toBool(),
                           settings.value("postHLOwnComments",     true).toBool(),
                           settings.value("postIgnoreSslInImages", false).toBool(),
                           settings.value("postFullImages",        false).toBool());


    ////////////////////////////////////////////////////////////////// COMPOSER
    this->syncComposerSettings(settings.value("publicPosts", false).toBool(),
                               settings.value("useFilenameAsTitle", false).toBool(),
                               settings.value("showCharacterCounter", false).toBool());


    /////////////////////////////////////////////////////////////////// PRIVACY
    this->syncPrivacySettings(settings.value("silentFollows", false).toBool(),
                              settings.value("silentLists", true).toBool(),
                              settings.value("silentLikes", false).toBool());


    ///////////////////////////////////////////////////////////// NOTIFICATIONS
    this->syncNotificationSettings(settings.value("notificationTaskbar",
                                                  true).toBool());
                                          // TODO: load the other nofification options


    ////////////////////////////////////////////////////////////////////// TRAY
    this->syncTrayOptions(settings.value("systrayHideInTray", false).toBool());
                                          // TODO: load the other tray options


    settings.endGroup();


    // Model for nick completion used by Composer
    this->nickCompletionModel = new QStandardItemModel(this);
    nickCompletionModel->setColumnCount(2);

    this->filterCompletionModel = new QSortFilterProxyModel(this);
    filterCompletionModel->setSortCaseSensitivity(Qt::CaseInsensitive);
    filterCompletionModel->setSourceModel(nickCompletionModel);


    // Timeline height used to calculate maximum optimal height for post contents
    this->timelineHeight = 400; // Some initial value


    this->programClosing = false;

    qDebug() << "GlobalObject created";
}

GlobalObject::~GlobalObject()
{
    qDebug() << "GlobalObject destroyed";
}




// General options
void GlobalObject::syncGeneralSettings()
{
    // TODO
}


// Font options
void GlobalObject::syncFontSettings(QString postTitleFont,
                                    QString postContentsFont,
                                    QString commentsFont,
                                    QString minorFeedFont)
{
    this->postTitleFontInfo = postTitleFont;
    this->postContentsFontInfo = postContentsFont;
    this->commentsFontInfo = commentsFont;
    this->minorFeedFontInfo = minorFeedFont;

    qDebug() << "GlobalObject::syncFontSettings() - font info sync'd";
}

QString GlobalObject::getPostTitleFont()
{
    return this->postTitleFontInfo;
}

QString GlobalObject::getPostContentsFont()
{
    return this->postContentsFontInfo;
}

QString GlobalObject::getCommentsFont()
{
    return this->commentsFontInfo;
}

QString GlobalObject::getMinorFeedFont()
{
    return this->minorFeedFontInfo;
}


// Color options
void GlobalObject::syncColorSettings(QStringList newColorList)
{
    this->colorsList = newColorList;

    qDebug() << "GlobalObject::syncColorSettings() - color list sync'd";
}

QStringList GlobalObject::getColorsList()
{
    return this->colorsList;
}

QString GlobalObject::getColor(int colorIndex)
{
    QString color;

    if (colorIndex >= 0 && colorIndex < this->colorsList.length())
    {
        color = this->colorsList.at(colorIndex);
        if (!QColor::isValidColor(color)) // If invalid, clear it
        {
            color.clear();
        }
    }

    return color;
}


// Timeline options
void GlobalObject::syncTimelinesSettings(int pppMain, int pppOther,
                                         bool showDeleted, bool hideDuplicates,
                                         bool jumpToNew, int minorFeedSnippets,
                                         int snippetsChars, int snippetsCharsHl,
                                         int mfAvatarIdx, int mfVerbIconType)
{
    this->postsPerPageMain = pppMain;
    this->postsPerPageOther = pppOther;

    this->showDeletedPosts = showDeleted;
    this->hideDuplicatedPosts = hideDuplicates;
    this->jumpToNewOnUpdate = jumpToNew;

    this->minorFeedSnippetsType = minorFeedSnippets;
    this->snippetsCharLimit = snippetsChars;
    this->snippetsCharLimitHl = snippetsCharsHl;
    this->mfAvatarIndex = mfAvatarIdx;
    this->mfAvatarSize = this->getAvatarSizeForIndex(mfAvatarIdx);

    this->mfIconType = mfVerbIconType;

    // TODO: more...
}

int GlobalObject::getPostsPerPageMain()
{
    return this->postsPerPageMain;
}

int GlobalObject::getPostsPerPageOther()
{
    return this->postsPerPageOther;
}

bool GlobalObject::getShowDeleted()
{
    return this->showDeletedPosts;
}

bool GlobalObject::getHideDuplicates()
{
    return this->hideDuplicatedPosts;
}

bool GlobalObject::getJumpToNewOnUpdate()
{
    return this->jumpToNewOnUpdate;
}

int GlobalObject::getMinorFeedSnippetsType()
{
    return this->minorFeedSnippetsType;
}

int GlobalObject::getSnippetsCharLimit()
{
    return this->snippetsCharLimit;
}

int GlobalObject::getSnippetsCharLimitHl()
{
    return this->snippetsCharLimitHl;
}

int GlobalObject::getMfAvatarIndex()
{
    return this->mfAvatarIndex;
}

QSize GlobalObject::getMfAvatarSize()
{
    return this->mfAvatarSize;
}

int GlobalObject::getMfIconType()
{
    return this->mfIconType;
}



// Post options
void GlobalObject::syncPostSettings(int postAvatarIdx, int commentAvatarIdx,
                                    bool extendedShares, bool showExtraInfo,
                                    bool hlAuthorComments, bool hlOwnComments,
                                    bool ignoreSslInImages, bool fullImages)
{
    this->postAvatarIndex = postAvatarIdx;
    this->postAvatarSize = this->getAvatarSizeForIndex(postAvatarIdx);
    this->commentAvatarIndex = commentAvatarIdx;
    this->commentAvatarSize = this->getAvatarSizeForIndex(commentAvatarIdx);

    this->postExtendedShares = extendedShares;
    this->postShowExtraInfo = showExtraInfo;
    this->postHLAuthorComments = hlAuthorComments;
    this->postHLOwnComments = hlOwnComments;
    this->postIgnoreSslInImages = ignoreSslInImages;
    this->postFullImages = fullImages;
}

int GlobalObject::getPostAvatarIndex()
{
    return this->postAvatarIndex;
}

QSize GlobalObject::getPostAvatarSize()
{
    return this->postAvatarSize;
}

int GlobalObject::getCommentAvatarIndex()
{
    return this->commentAvatarIndex;
}

QSize GlobalObject::getCommentAvatarSize()
{
    return this->commentAvatarSize;
}

bool GlobalObject::getPostExtendedShares()
{
    return this->postExtendedShares;
}

bool GlobalObject::getPostShowExtraInfo()
{
    return this->postShowExtraInfo;
}

bool GlobalObject::getPostHLAuthorComments()
{
    return this->postHLAuthorComments;
}

bool GlobalObject::getPostHLOwnComments()
{
    return this->postHLOwnComments;
}

bool GlobalObject::getPostIgnoreSslInImages()
{
    return this->postIgnoreSslInImages;
}

bool GlobalObject::getPostFullImages()
{
    return this->postFullImages;
}


// Composer options
void GlobalObject::syncComposerSettings(bool publicPosts,
                                        bool filenameAsTitle,
                                        bool showCharCounter)
{
    this->publicPostsByDefault = publicPosts;
    this->useFilenameAsTitle = filenameAsTitle;
    this->showCharacterCounter = showCharCounter;
}

bool GlobalObject::getPublicPostsByDefault()
{
    return this->publicPostsByDefault;
}

bool GlobalObject::getUseFilenameAsTitle()
{
    return this->useFilenameAsTitle;
}

bool GlobalObject::getShowCharacterCounter()
{
    return this->showCharacterCounter;
}



// Privacy options
void GlobalObject::syncPrivacySettings(bool silentFollows, bool silentLists,
                                       bool silentLikes)
{
    this->silentFollowing = silentFollows;
    this->silentListsHandling = silentLists;
    this->silentLiking = silentLikes;
}

bool GlobalObject::getSilentFollows()
{
    return this->silentFollowing;
}

bool GlobalObject::getSilentLists()
{
    return this->silentListsHandling;
}

bool GlobalObject::getSilentLikes()
{
    return this->silentLiking;
}



// Notification options
void GlobalObject::syncNotificationSettings(bool notifyTaskbar)
{
    this->notifyInTaskbar = notifyTaskbar;

    // TODO, most still handled elsewhere
}

bool GlobalObject::getNotifyInTaskbar()
{
    return this->notifyInTaskbar;
}



// Tray options
void GlobalObject::syncTrayOptions(bool hideInTrayStartup)
{
    this->hideInTray = hideInTrayStartup;

    // TODO, some still handled elsewhere
}


bool GlobalObject::getHideInTray()
{
    return this->hideInTray;
}

///////////////////////////////////////////////////////////////////////////////


void GlobalObject::setDataDirectory(QString dataDir)
{
    this->dataDirectory = dataDir;
}

QString GlobalObject::getDataDirectory()
{
    return this->dataDirectory;
}


void GlobalObject::createMessageForContact(QString id, QString name,
                                           QString url)
{
    // Send signal to be caught by Publisher()
    emit messagingModeRequested(id, name, url);

    qDebug() << "GlobalObject; asking for Messaging mode for "
             << name << id << url;
}



void GlobalObject::browseUserMessages(QString userId, QString userName,
                                      QIcon userAvatar, QString userOutbox)
{
    // Signal to be caught by MainWindow
    emit userTimelineRequested(userId, userName,
                               userAvatar, userOutbox);
}



void GlobalObject::editPost(QString originalPostId,
                            QString type,
                            QString title,
                            QString contents)
{
    // Signal to be caught by Publisher
    emit postEditRequested(originalPostId,
                           type,
                           title,
                           contents);

    qDebug() << "GlobalObject; asking to edit post: "
             << originalPostId << title << type;
}



QSortFilterProxyModel *GlobalObject::getNickCompletionModel()
{
    return this->filterCompletionModel;
}


void GlobalObject::addToNickCompletionModel(QString id, QString name,
                                            QString url)
{
    QStandardItem *itemName = new QStandardItem(name);
    itemName->setData(id,  Qt::UserRole + 1);
    itemName->setData(url, Qt::UserRole + 2);

    QStandardItem *itemId = new QStandardItem(id);

    QList<QStandardItem*> itemLine;
    itemLine.append(itemName);
    itemLine.append(itemId);

    this->nickCompletionModel->appendRow(itemLine);
}

void GlobalObject::removeFromNickCompletionModel(QString id)
{
    qDebug() << "GlobalObject::removeFromNickCompletionModel()" << id;

    QList<QStandardItem *> allNicks;
    allNicks = nickCompletionModel->findItems(QString(), Qt::MatchContains);

    foreach (QStandardItem *item, allNicks)
    {
        //qDebug() << item->data(Qt::UserRole + 1).toString();

        if (item->data(Qt::UserRole + 1).toString() == id)
        {
            int rowNum = item->row();
            /*
             * FIXME: Ensure deleting the item is not needed.
             * Deleting it before removeRow doesn't seem to do any harm.
             * removeRow seems to delete it anyway; trying to delete it
             * afterwards results in segfault.
             *
             * delete item;
             */
            this->nickCompletionModel->removeRow(rowNum);
        }
    }
}

void GlobalObject::clearNickCompletionModel()
{
    // clear() seems to delete the items properly
    this->nickCompletionModel->clear();
}

void GlobalObject::sortNickCompletionModel()
{
    this->filterCompletionModel->sort(0);
}

/*
 * Return a Display Name + URL pair for a given user ID,
 * to be used when restoring audience from a draft
 *
 */
QPair<QString, QString> GlobalObject::getDataForNick(QString id)
{
    QList<QStandardItem *> matchingNicks;
    matchingNicks = nickCompletionModel->findItems(id, Qt::MatchExactly, 1);
    if (matchingNicks.isEmpty())
    {
        // Return basic data for invalid case
        return QPair<QString,QString>(id, QString());
    }

    QStandardItem *item = nickCompletionModel->item(matchingNicks.first()->row(),
                                                    0);
    // FIXME: add some error control

    return QPair<QString,QString>(item->text(),
                                  item->data(Qt::UserRole + 2).toString());
}


/*
 * Change status bar message in main window
 *
 */
void GlobalObject::setStatusMessage(QString message)
{
    emit messageForStatusBar(message);
}

/*
 * Add a log message to the log viewer
 *
 */
void GlobalObject::logMessage(QString message, QString url)
{
    emit messageForLog(message, url);
}

void GlobalObject::storeTimelineHeight(int height)
{
    // Substract some pixels to account for the row of buttons, etc.
    this->timelineHeight = qMax(height - 80,
                                50); // Never less than 50, but window should never be that small
}

int GlobalObject::getTimelineHeight()
{
    return this->timelineHeight;
}

/*
 * Get corresponding QSize for specified index from combo box
 *
 */
QSize GlobalObject::getAvatarSizeForIndex(int index)
{
    int pixelSize;

    switch (index)
    {
    case 0:
        pixelSize = 16;
        break;
    case 1:
        pixelSize = 32;
        break;
    case 2:
        pixelSize = 48;
        break;
    // case 3 = default = 64
    case 4:
        pixelSize = 96;
        break;
    case 5:
        pixelSize = 128;
        break;
    case 6:
        pixelSize = 256;
        break;

    default: // index = 3 or invalid option
        pixelSize = 64;
    }

    return QSize(pixelSize, pixelSize);
}



void GlobalObject::notifyProgramShutdown()
{
    this->programClosing = true;
    emit programShuttingDown();
}

bool GlobalObject::isProgramShuttingDown()
{
    return this->programClosing;
}
