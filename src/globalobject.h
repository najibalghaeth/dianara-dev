/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef GLOBALOBJECT_H
#define GLOBALOBJECT_H

#include <QObject>
#include <QSettings>
#include <QFont>
#include <QStringList>
#include <QSize>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>

#include <QDebug>


class GlobalObject : public QObject
{
    Q_OBJECT

public:
    explicit GlobalObject(QObject *parent = 0);
    ~GlobalObject();


    // General options
    void syncGeneralSettings();

    // Font options
    void syncFontSettings(QString postTitleFont,
                          QString postContentsFont,
                          QString commentsFont,
                          QString minorFeedFont);
    QString getPostTitleFont();
    QString getPostContentsFont();
    QString getCommentsFont();
    QString getMinorFeedFont();


    // Color options
    void syncColorSettings(QStringList newColorList);
    QStringList getColorsList();
    QString getColor(int colorIndex);


    // Timeline options
    void syncTimelinesSettings(int pppMain, int pppOther,
                               bool showDeleted, bool hideDuplicates,
                               bool jumpToNew, int minorFeedSnippets,
                               int snippetsChars, int snippetsCharsHl,
                               int mfAvatarIdx, int mfVerbIconType);
    int getPostsPerPageMain();
    int getPostsPerPageOther();
    bool getShowDeleted();
    bool getHideDuplicates();
    bool getJumpToNewOnUpdate();
    int getMinorFeedSnippetsType();
    int getSnippetsCharLimit();
    int getSnippetsCharLimitHl();
    int getMfAvatarIndex();
    QSize getMfAvatarSize();
    int getMfIconType();



    // Post options
    void syncPostSettings(int postAvatarIdx, int commentAvatarIdx,
                          bool extendedShares, bool showExtraInfo,
                          bool hlAuthorComments, bool hlOwnComments,
                          bool postIgnoreSslInImages, bool fullImages);
    int getPostAvatarIndex();
    QSize getPostAvatarSize();
    int getCommentAvatarIndex();
    QSize getCommentAvatarSize();
    bool getPostExtendedShares();
    bool getPostShowExtraInfo();
    bool getPostHLAuthorComments();
    bool getPostHLOwnComments();
    bool getPostIgnoreSslInImages();
    bool getPostFullImages();


    // Composer options
    void syncComposerSettings(bool publicPosts, bool filenameAsTitle,
                              bool showCharCounter);
    bool getPublicPostsByDefault();
    bool getUseFilenameAsTitle();
    bool getShowCharacterCounter();


    // Privacy options
    void syncPrivacySettings(bool silentFollows, bool silentLists,
                             bool silentLiking);
    bool getSilentFollows();
    bool getSilentLists();
    bool getSilentLikes();


    // Notification options
    void syncNotificationSettings(bool notifyTaskbar);
    bool getNotifyInTaskbar();


    // Tray options
    void syncTrayOptions(bool hideInTrayStartup);
    bool getHideInTray();


    ///////////////////////////////////////////////////////////////////////////

    void setDataDirectory(QString dataDir);
    QString getDataDirectory();

    void createMessageForContact(QString id, QString name, QString url);

    void browseUserMessages(QString userId, QString userName,
                            QIcon userAvatar, QString userOutbox);

    void editPost(QString originalPostId,
                  QString type,
                  QString title,
                  QString contents);

    QSortFilterProxyModel *getNickCompletionModel();
    void addToNickCompletionModel(QString id, QString name, QString url);
    void removeFromNickCompletionModel(QString id);
    void clearNickCompletionModel();
    void sortNickCompletionModel();
    QPair<QString,QString> getDataForNick(QString id);

    void setStatusMessage(QString message);
    void logMessage(QString message, QString url="");

    void storeTimelineHeight(int height);
    int getTimelineHeight();

    QSize getAvatarSizeForIndex(int index);

    void notifyProgramShutdown();
    bool isProgramShuttingDown();


signals:
    void messagingModeRequested(QString id, QString name, QString url);

    void userTimelineRequested(QString userId, QString userName,
                               QIcon userAvatar, QString userOutbox);

    void postEditRequested(QString originalPostId,
                           QString type,
                           QString title,
                           QString contents);

    void messageForStatusBar(QString message);
    void messageForLog(QString message, QString url);

    void programShuttingDown();

public slots:


private:
    // General options

    //// (not handled here yet)


    // Font options
    QString postTitleFontInfo;
    QString postContentsFontInfo;
    QString commentsFontInfo;
    QString minorFeedFontInfo;


    // Color options
    QStringList colorsList;


    // Timeline options
    int postsPerPageMain;
    int postsPerPageOther;
    bool showDeletedPosts;
    bool hideDuplicatedPosts;
    bool jumpToNewOnUpdate;
    int minorFeedSnippetsType;
    int snippetsCharLimit;
    int snippetsCharLimitHl;
    int mfAvatarIndex;
    QSize mfAvatarSize;
    int mfIconType;


    // Post options
    int postAvatarIndex;
    QSize postAvatarSize;
    int commentAvatarIndex;
    QSize commentAvatarSize;
    bool postExtendedShares;
    bool postShowExtraInfo;
    bool postHLAuthorComments;
    bool postHLOwnComments;
    bool postIgnoreSslInImages;
    bool postFullImages;


    // Composer options
    bool publicPostsByDefault;
    bool useFilenameAsTitle;
    bool showCharacterCounter;


    // Privacy options
    bool silentFollowing;
    bool silentListsHandling;
    bool silentLiking;


    // Notification options
    bool notifyInTaskbar;


    // Tray options
    bool hideInTray;


    //////////////////////////////////////////////////////////////////////////

    // Other stuff
    QString dataDirectory;

    QStandardItemModel *nickCompletionModel;
    QSortFilterProxyModel *filterCompletionModel;
    int timelineHeight;

    bool programClosing;
};

#endif // GLOBALOBJECT_H
