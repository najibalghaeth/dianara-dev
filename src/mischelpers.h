/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef MISCHELPERS_H
#define MISCHELPERS_H

#include <QObject>
#include <QString>
#include <QRegExp>
#include <QStringList>
#include <QPixmap>
#include <QFileInfo>
#include <QTextDocument>
#include <QImageReader>
#include <QMovie>
#include <QStandardPaths>
#include <QMimeDatabase>
#include <QDesktopServices>
#include <QMessageBox>


#include <QDebug>

class MiscHelpers : public QObject
{
    Q_OBJECT

public:
    explicit MiscHelpers(QObject *parent = 0);

    static QString getCachedAvatarFilename(QString url);
    static QString getCachedImageFilename(QString url);

    static QString getSuggestedFilename(QString authorId, QString postType,
                                        QString postTitle, QString fileUrl,
                                        QString mimeType="");

    static QString getFileMimeType(QString fileUri);
    static int getImageWidth(QString fileURI);
    static bool isImageAnimated(QString fileUri);
    static QStringList iconsForActivity(QString verb);

    static const QStringList imageExtensions();
    static const QStringList audioExtensions();
    static const QStringList videoExtensions();
    static const QString fileFilterString(QStringList extensions);

    static QString fixLongName(QString name);

    static QString fileSizeString(QString fileURI);
    static QString resolutionString(int width, int height);

    static QStringList htmlWithReplacedImages(QString originalHtml,
                                              int postWidth);
    static QString cleanupHtml(QString originalHtml);
    static QString htmlWithoutLinks(QString originalHtml);
    static QString htmlToPlainText(QString html, int charLimit=0);

    static QString quotedText(QString author, QString content);

    static QString elidedText(QString text, int charLimit);

    static QString mediaHtmlBase(QString postType, QString attachmentFilename,
                                 QString tooltipMessage, QString belowMessage,
                                 int imageWidth = -1);

    static bool openUrl(QUrl url, QWidget *parentWidget);
};

#endif // MISCHELPERS_H
