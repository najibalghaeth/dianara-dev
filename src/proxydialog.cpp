/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "proxydialog.h"


ProxyDialog::ProxyDialog(int proxyType,
                         QString hostname, QString port,
                         bool useAuth,
                         QString user, QString password,
                         QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle(tr("Proxy Configuration") + " - Dianara");
    this->setWindowIcon(QIcon::fromTheme("preferences-system-network",
                                         QIcon(":/images/button-configure.png")));
    this->setWindowFlags(Qt::Dialog);
    this->setWindowModality(Qt::WindowModal);
    this->setMinimumSize(460, 320);

    m_proxyTypeComboBox = new QComboBox(this);
    m_proxyTypeComboBox->addItem(tr("Do not use a proxy"));
    m_proxyTypeComboBox->addItem("SOCKS 5");
    m_proxyTypeComboBox->addItem("HTTP");
    m_proxyTypeComboBox->setCurrentIndex(proxyType);

    m_hostnameLineEdit = new QLineEdit(hostname, this);
    m_hostnameLineEdit->setPlaceholderText("example.org");

    m_portLineEdit = new QLineEdit(port, this);
    m_portLineEdit->setPlaceholderText("1080, 8080..."); // Defaults for socks5 and http

    m_authCheckBox = new QCheckBox(this);
    m_authCheckBox->setChecked(useAuth);
    connect(m_authCheckBox, &QAbstractButton::toggled,
            this, &ProxyDialog::toggleAuth);

    m_userLineEdit = new QLineEdit(user, this);
    m_userLineEdit->setPlaceholderText(tr("Your proxy username"));

    m_passwordLineEdit = new QLineEdit(password, this);
    m_passwordLineEdit->setEchoMode(QLineEdit::Password);

    m_passwordNoteLabel = new QLabel(tr("Note: Password is not stored in a "
                                        "secure manner. If you wish, you can "
                                        "leave the field empty, and you'll be "
                                        "prompted for the password on startup."),
                                     this);
    m_passwordNoteLabel->setWordWrap(true);
    QFont noteFont;
    noteFont.setPointSize(noteFont.pointSize() - 1);
    m_passwordNoteLabel->setFont(noteFont);

    this->toggleAuth(useAuth); // Enable or disable initially

    // Bottom
    m_saveButton = new QPushButton(QIcon::fromTheme("document-save",
                                                    QIcon(":/images/button-save.png")),
                                   tr("&Save"),
                                   this);
    connect(m_saveButton, &QAbstractButton::clicked,
            this, &ProxyDialog::saveSettings);

    m_cancelButton = new QPushButton(QIcon::fromTheme("dialog-cancel",
                                                      QIcon(":/images/button-cancel.png")),
                                     tr("&Cancel"),
                                     this);
    connect(m_cancelButton, &QAbstractButton::clicked,
            this, &QWidget::hide);


    // ESC to close
    m_closeAction = new QAction(this);
    m_closeAction->setShortcut(QKeySequence(Qt::Key_Escape));
    connect(m_closeAction, &QAction::triggered,
            this, &QWidget::hide);
    this->addAction(m_closeAction);


    //////////////////////////////////////////// Layout
    m_fieldsLayout = new QFormLayout();
    m_fieldsLayout->addRow(tr("Proxy &Type"),
                           m_proxyTypeComboBox);
    m_fieldsLayout->addRow(tr("&Hostname"),
                           m_hostnameLineEdit);
    m_fieldsLayout->addRow(tr("&Port"),
                           m_portLineEdit);
    m_fieldsLayout->addRow(tr("Use &Authentication"),
                           m_authCheckBox);
    m_fieldsLayout->addRow(tr("&User"),
                           m_userLineEdit);
    m_fieldsLayout->addRow(tr("Pass&word"),
                           m_passwordLineEdit);
    m_fieldsLayout->addRow(QString(),
                           m_passwordNoteLabel);

    m_buttonsLayout = new QHBoxLayout();
    m_buttonsLayout->setAlignment(Qt::AlignRight);
    m_buttonsLayout->addWidget(m_saveButton);
    m_buttonsLayout->addWidget(m_cancelButton);

    m_mainLayout = new QVBoxLayout();
    m_mainLayout->addLayout(m_fieldsLayout);
    m_mainLayout->addSpacing(16);
    m_mainLayout->addStretch(1);
    m_mainLayout->addLayout(m_buttonsLayout);
    this->setLayout(m_mainLayout);

    qDebug() << "ProxyDialog created";
}


ProxyDialog::~ProxyDialog()
{
    qDebug() << "ProxyDialog destroyed";
}


//////////////////////////////////////////////////////////////////////////////
///////////////////////////////// SLOTS //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


void ProxyDialog::toggleAuth(bool state)
{
    m_userLineEdit->setEnabled(state);
    m_passwordLineEdit->setEnabled(state);
}


void ProxyDialog::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Configuration");
    settings.setValue("proxyType",     m_proxyTypeComboBox->currentIndex());
    settings.setValue("proxyHostname", m_hostnameLineEdit->text());
    settings.setValue("proxyPort",     m_portLineEdit->text());

    settings.setValue("proxyUseAuth",  m_authCheckBox->isChecked());
    if (!m_authCheckBox->isChecked()) // If no auth, clear saved username/passwd
    {
        m_userLineEdit->clear();
        m_passwordLineEdit->clear();
    }

    settings.setValue("proxyUser",     m_userLineEdit->text());
    // VERY TMP: Saving passwd as base64 for now; FIXME
    settings.setValue("proxyPassword", m_passwordLineEdit->text().toUtf8()
                                                                 .toBase64());
    settings.endGroup();

    this->hide();

    qDebug() << "ProxyDialog::saveSettings()"
             << m_hostnameLineEdit->text() << m_portLineEdit->text();
}
