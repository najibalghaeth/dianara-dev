/*
 *   This file is part of Dianara
 *   Copyright 2012-2018  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "siteuserslist.h"


SiteUsersList::SiteUsersList(PumpController *pumpController,
                             GlobalObject *globalObject,
                             QWidget *parent) : QWidget(parent)
{
    m_pumpController = pumpController;

    m_explanationLabel = new QLabel(tr("You can get a list of the newest "
                                       "users registered on your server "
                                       "by clicking the button below."),
                                    this);
    m_explanationLabel->setWordWrap(true);
    m_explanationLabel->setAlignment(Qt::AlignTop | Qt::AlignHCenter);
    m_explanationLabel->setMargin(8);


    m_getListButton = new QPushButton(QIcon::fromTheme("system-users",
                                                       QIcon(":/images/button-users.png")),
                                      tr("Get list of users from your server"),
                                      this);
    connect(m_getListButton, &QAbstractButton::clicked,
            this, &SiteUsersList::getList);


    m_infoLinksLabel = new QLabel(tr("More resources to find users:")
                                  + "<ul>"
                                    "<li>"
                                    "<a href=\"https://github.com/pump-io/pump.io/wiki/Users-by-language\">"
                                  + tr("Wiki page 'Users by language'")
                                  + "</a>."
                                    "</li>"
                                    "<li>"
                                    "<a href=\"https://www.inventati.org/ppump/usuarios/\">"
                                  + tr("PPump user search service at inventati.org")
                                  + "</a>."
                                    "</li>"
                                    "<li>"
                                    "<a href=\"https://pumpit.info/pumpio/followers\">"
                                  + tr("List of Followers for the Pump.io Community account")
                                  + "</a>."
                                    "</li>"
                                    "</ul>",
                                  this);
    m_infoLinksLabel->setWordWrap(true);
    m_infoLinksLabel->setAlignment(Qt::AlignTop);
    m_infoLinksLabel->setMargin(8);
    m_infoLinksLabel->setOpenExternalLinks(true);



    m_serverInfoLabel = new QLabel(this);
    m_serverInfoLabel->setWordWrap(true);
    m_serverInfoLabel->setAlignment(Qt::AlignCenter);
    m_serverInfoLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
    m_serverInfoLabel->setMargin(4);
    m_serverInfoLabel->hide();

    m_closeListButton = new QPushButton(QIcon::fromTheme("window-close",
                                                         QIcon(":/images/button-close.png")),
                                        QString(),
                                        this);
    m_closeListButton->setSizePolicy(QSizePolicy::Maximum,
                                     QSizePolicy::Maximum);
    m_closeListButton->setShortcut(QKeySequence("Ctrl+W"));
    m_closeListButton->setToolTip(tr("Close list")
                                  + " ("
                                  + m_closeListButton->shortcut()
                                    .toString(QKeySequence::NativeText)
                                  + ")");
    connect(m_closeListButton, &QAbstractButton::clicked,
            this, &SiteUsersList::closeList);
    m_closeListButton->hide();


    m_userList = new ContactList(m_pumpController,
                                 globalObject,
                                 QStringLiteral("site-users"),
                                 this);
    m_userList->hide();
    connect(m_pumpController, &PumpController::siteUserListReceived,
            this, &SiteUsersList::setListContents);


    m_middleLayout = new QHBoxLayout();
    m_middleLayout->addWidget(m_serverInfoLabel);
    m_middleLayout->addWidget(m_closeListButton);


    m_mainLayout = new QVBoxLayout();
    m_mainLayout->setContentsMargins(0, 0, 0, 0);
    m_mainLayout->addWidget(m_explanationLabel, 0, Qt::AlignVCenter);
    m_mainLayout->addWidget(m_getListButton,    0, Qt::AlignTop | Qt::AlignHCenter);
    m_mainLayout->addWidget(m_infoLinksLabel,   0, Qt::AlignTop);
    m_mainLayout->addLayout(m_middleLayout);
    m_mainLayout->addWidget(m_userList);
    this->setLayout(m_mainLayout);

    qDebug() << "SiteUsersList created";
}

SiteUsersList::~SiteUsersList()
{
    qDebug() << "SiteUsersList destroyed";
}


/*****************************************************************************/
/*********************************** SLOTS ***********************************/
/*****************************************************************************/


void SiteUsersList::getList()
{
    m_pumpController->getSiteUserList();

    m_explanationLabel->hide();
    m_getListButton->hide();
    m_infoLinksLabel->hide();


    m_serverInfoLabel->setText(tr("Loading..."));
    m_serverInfoLabel->show();
    m_closeListButton->show();

    m_userList->clearListContents();
    m_userList->show();
}


void SiteUsersList::setListContents(QVariantList userList, int totalUsers)
{
    m_serverInfoLabel->setText(tr("%1 users in %2",
                                  "%1 = user count, %2 = server name")
                               .arg(QLocale::system().toString(totalUsers))
                               .arg(m_pumpController->currentServerDomain()));

    m_userList->setListContents(userList);
}


void SiteUsersList::closeList()
{
    m_explanationLabel->show();
    m_getListButton->show();
    m_infoLinksLabel->show();

    m_serverInfoLabel->hide();
    m_closeListButton->hide();

    m_userList->clearListContents();
    m_userList->hide();
}

